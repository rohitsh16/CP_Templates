```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 500005;

vector<vector<int> > trie(27, vector<int>(MAX_N, 0));
vector<bool> vis(MAX_N, 0);
vector<int> last(MAX_N, 0);
int sz=0;

void insert(string &s){
	int n = s.size(), v = 0;
	for(int i=0;i<n;i++){
		int c = s[i]-'a';
		if(!vis[trie[c][v]]){
			trie[c][v] = ++sz;
			vis[sz] = 1;
		}
		v = trie[c][v];
	}
	++last[v];
}

bool search(string &s){
	int n = s.length(), v = 0;
	for(int i=0;i<n;i++){
		int c = s[i]-'a';
		if(!vis[trie[c][v]])
			return 0;
		v = trie[c][v];
	}
	return (last[v]>0);
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t=1;
  // cin>>t;
  while(t--){
  	int n; cin>>n;
    vector<string> s(n);
    for(int i=0;i<n;i++){
    	cin>>s[i];
    	insert(s[i]);
    }
   	string temp;
   	cin>>temp;
   	if(search(temp))
   		cout<<"Found\n";
   	else
   		cout<<"Not Found\n";
  }
  return 0;
}
