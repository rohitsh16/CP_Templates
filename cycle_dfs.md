```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 200005;

vector<vector<int> > adj(MAX_N);
vector<int> color(MAX_N,0);
vector<int> par; int cycle_start, cycle_end;

bool dfs(int s, int p=-1){
  color[s]=1;
  for(auto i:adj[s]){
    if(!color[i]){
      par[i]=s;
      if(dfs(i, s))
        return 1;
    }
    else if(i!=p&&color[i]==1){
      cycle_end=s;
      cycle_start=i;
      return 1;
    }
  }
  color[s]=2;
  return 0;
}

int findcycle(int n){
  par.assign(n,-1);
  cycle_start=-1;
  for(int i=0;i<n;i++){
    if(!color[i]&&dfs(i))
      break;
  }
  if(cycle_start==-1)
    return cout<<"IMPOSSIBLE\n", 0;
  vector<int> cycle;
  cycle.eb(cycle_start+1);
  int i=cycle_end;
  while(i!=cycle_start){
    cycle.eb(i+1);
    i=par[i];
  }
  cycle.eb(cycle_start+1);
  cout<<cycle.size()<<"\n";
  for(auto i:cycle)
    cout<<i<<" ";
  cout<<"\n";
  return 0;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t=1;
  // cin>>t;
  while(t--){
    int n,m; cin>>n>>m;
    for(int i=0;i<m;i++){
      int x,y; cin>>x>>y;
      x--, y--;
      adj[x].eb(y);
      adj[y].eb(x);
    }
    findcycle(n);
  }
  return 0;
}
