```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 1000005;

ll power(ll x, ll y){
    ll res = 1;
    while(y){
        if(y&1)
          res = (res*x)%mod;
        x = (x*x)%mod;
        y >>= 1;
    }
    return (res%mod);
}

ll ncr(ll n, ll r){
    ll p = 1, k = 1;
    r = (n-r)<r?(n-r):r;
    if(r==0)
        return p;
    while(r){
        p = p*n;
        k *= r;
        ll m = __gcd(p,k);
        p /= m; k /= m;
        n--; r--;
    }
    return p;
}

ll modInverse(ll n){
  return power(n, mod-2);
}

ll ncrfermet(ll n, ll r){
  if(r==0)
    return 1;
  vector<ll> fact(n+1);
  fact[0] = 1;
  for(ll i=1; i<=n; i++)
    fact[i] = fact[i-1]*i%mod;
  ll res = (fact[n]*modInverse(fact[r])%mod*modInverse(fact[n-r])%mod)%mod;
  return (res%mod);
}

void solve(){
  
  return ;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t = 1;
  // cin>>t;
  while(t--){
    solve();
  }
  return 0;
}
