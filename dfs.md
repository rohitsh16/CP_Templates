```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 200005;
     
vector<vector<int> > adj(MAX_N);
vector<int> stk(MAX_N, 0);
vector<int> par(MAX_N, -1);
vector<int> height(MAX_N, 0);
int cycle_start = -1, cycle_end = -1;
     
bool dfs(int s, int parent){
  stk[s] = 1;
  if(cycle_start!=-1)
    return 1;
  for(auto i: adj[s]){
    if(i==parent)
      continue;
    if(!stk[i]){
      par[i] = s;
      height[i] = height[s]+1;
    if(dfs(i, par[i]))
      return 1;
    }
    else if(stk[i]==1){
      cycle_start = i;
      cycle_end = s;
      return 1;
    }
  }
  stk[s] = 2;
  return 0;
}

void solve(){
  int n, m, k; cin>>n>>m>>k;
  for(int i=0; i<m; i++){
    int u, v; cin>>u>>v;
    u--, v--;
    if(u>=k||v>=k)
      continue;
    adj[u].eb(v);
    adj[v].eb(u);
  }
  for(int i=0; i<k; i++){
    if(!stk[i])
      dfs(i, par[i]);
  }
  // dfs(0, par[0]);
  vector<int> cycle;
  if(cycle_start==-1){
    vector<int> ans[2];
    for(int i=0; i<k; i++){
      ans[height[i]&1].eb(i+1);
    }
    if(ans[0].size()<ans[1].size())
      swap(ans[0],ans[1]);
    cout<<"1\n";
    for(int i=0; i<(k+1)/2; i++)
      cout<<ans[0][i]<<" ";
    cout<<"\n";
      return;
    }
    cout<<"2\n";
    cycle.eb(cycle_start);
    int i = cycle_end;
    // cout<<cycle_start<<" "<<cycle_end<<"\n";
  while(i!=cycle_start){
    cycle.eb(i);
    i = par[i];
  }
  reverse(all(cycle));
  cout<<cycle.size()<<"\n";
  for(auto i: cycle)
    cout<<i+1<<" ";
  return ;
}
/* Why code Fails if cycle_start!=-1 isn't put in DFS.
  Take C
  7 8 6
  6 7
  1 2
  1 3
  2 4
  3 5
  4 6
  5 7
  2 3
*/
int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t=1;
  // cin>>t;
  while(t--){
    solve();
  }
  return 0;
}
