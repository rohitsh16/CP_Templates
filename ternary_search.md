```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 1000005;

vector<vector<int> > adj;
vector<bool> vis;

int ternary_search(vector<int> &a, int n){
  int l = -1, h = n-1;
  while(h-l>1){
    int m = (l+h)>>1;
    if(a[m]>a[m+1])
      h=m;
    else
      l=m;
  }
  // For first Increasing and the Deacreasing function
  // Look for : 7
  //         :  7 1 2 3 4 5 6
  return l+1;
}

void solve(){
  int n ; cin>>n;
  vector<int> a(n);
  for(int i=0; i<n; i++)
    cin>>a[i];
  cout<<ternary_search(a, n)<<"\n";
  return ;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t=1;
  // cin>>t;
  while(t--){
    solve();
  }
  return 0;
}
