```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 1000005;

struct Matrix{
    double mat[2][2] = {{0,0},{0,0}};
    Matrix operator *(const Matrix& other){
        Matrix prod;
        for(int i=0;i<2;i++){
            for(int j=0;j<2;j++)
                for(int k=0;k<2;k++)
                    prod.mat[i][k]+=mat[i][j]*other.mat[j][k];
        }
        return prod;
    }
};

Matrix power(Matrix x, int y){
    Matrix ans;
    for(int i=0;i<2;i++)
        ans.mat[i][i]=1;
    while(y){
        if(y%2)
            ans=ans*x;
        x=x*x;
        y=y>>1;
    }
    return ans;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t=1;
  // cin>>t;
  while(t--){
    int n; cin>>n;
    double p; cin>>p;
    Matrix temp;
    temp.mat[0][0]=1-p;
    temp.mat[0][1]=p;
    temp.mat[1][0]=p;
    temp.mat[1][1]=1-p;
    Matrix ans=power(temp, n);
    cout<<fixed<<setprecision(10)<<ans.mat[0][0]<<"\n";
  }
  return 0;
}
