```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 200005;

int n;
vector<vector<int> > adj(MAX_N);
vector<bool> vis(MAX_N, 0);
vector<int> sz(MAX_N, 0);

void dfs(int s, int p=0){
  vis[s]=1;
  sz[s]=1;
  for(auto i:adj[s]){
    if(i!=p){
      dfs(i, s);
      sz[s]+=sz[i];
    }
  }
  return ;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t=1;
  // cin>>t;
  while(t--){
    cin>>n;
    for(int i=1;i<n;i++){
      int x; cin>>x; x--;
      adj[x].eb(i);
      adj[i].eb(x);
    }
    dfs(0);
    // cout<<n-1<<" ";
    for(int i=0;i<n;i++)
      cout<<sz[i]-1<<" ";
    cout<<"\n";
  }
  return 0;
}
