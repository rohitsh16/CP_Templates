```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 1000005;

vector<vector<int> > adj;
vector<bool> vis;

vector<int> par(201, -1);

int root(int u){
  if(par[u]<0)
    return u;
  par[u]=root(par[u])
  ; return par[u];
}

int merge(int u, int v){
  u=root(u);
  v=root(v);
  if(u==v)
    return 0;
  if(par[u]<par[v])
    par[v]=u;
  else if(par[u]>par[v])
    par[u]=v;
  else
    par[u]=v, par[v]--;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t=1;
  // cin>>t;
  while(t--){
    int n,m; cin>>n>>m;
    set<int> s; vector<int> a(201);
    int ans=0;
    for(int i=0;i<n;i++){
      int k; cin>>k;
      if(k==0){
        ans++;
        continue;
      }
      for(int j=0;j<k;j++){
        cin>>a[j];
        s.insert(a[j]);
      }
      for(int j=1;j<k;j++)
        merge(a[j-1], a[j]);
    }
    int cnt=0;
    for(int i=1;i<=m;i++){
      if(par[i]<0&&s.find(i)!=s.end())
        cnt++;
    }

    cout<<ans+max(0, cnt-1)<<"\n";
  }
  return 0;
}
