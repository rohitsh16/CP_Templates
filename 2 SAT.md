```cpp
#include <bits/stdc++.h>
using namespace std;
#define ll long long int
#define all(x) x.begin(), x.end()
#define eb(x) emplace_back(x)
#define MAX 200000

bool visited[200005];
int scc[200005], cnt = 1;
vector<int> adj[200005], adjr[200005];
stack<int> st;
int n, m;
map<int, int> mp;

void debug(int n){
  for(int i=1; i<=n; i++)
    printf("%d -> %d and %d -> %d\n",i,scc[i],mp[i],scc[mp[i]]);
}

void dfsFirst(ll curr){
  if(visited[curr])
    return;
  visited[curr]=true;
  for(int i=0; i<(int)adj[curr].size(); i++)
    dfsFirst(adj[curr][i]);
  st.push(curr);
  return ;
}

void dfsSecond(ll curr){
  if(visited[curr])
    return;
  visited[curr] = true;
  for(int i=0; i<(int)adjr[curr].size(); i++)
    dfsSecond(adjr[curr][i]);
  scc[curr]=cnt;
  return ;
}

bool Scc(){
  cnt = 1;
  for(int i=1; i<=2*n; i++)
    dfsFirst(i);
  memset(visited, 0, sizeof(visited));
  while(!st.empty()) {
    if(visited[st.top()]){
      st.pop();
      continue;
    }
    dfsSecond(st.top());
    st.pop();
    cnt++;
  }
  for(int i=1; i<=n; i++){
    if(scc[i]==scc[mp[i]])
      return false;
  }
  return true;
}

void input(){
  cin>>n>>m;
  set<pair<int, int>> s;
  for(int i=0; i<n; i++){
    int x, y; cin>>x>>y;
    mp[x] = y;
    mp[y] = x;
  }
  for(int i=0; i<m; i++){
    ll x, y, negx, negy; cin>>x>>y;
    negx = mp[x];       // complement of x
    negy = mp[y];       // complement of y
    // relation b/w x and y i.e XOR, AND, OR
    adj[negx].eb(y);
    adj[negy].eb(x);
    adj[x].eb(negy);
    adj[y].eb(negx);

    adjr[y].eb(negx);
    adjr[x].eb(negy);
    adjr[negx].eb(y);
    adjr[negy].eb(x);
  }
  return ;
}

void solve(){
  mp.clear();
  input();
  bool res = Scc();
  for(int i=1; i<=2*n+4; i++){
    adj[i].clear(),adjr[i].clear();
    visited[i] = 0;
    scc[i] = 0;
  }
  while(!st.empty())
    st.pop();
  if(res)
    cout<<"CHANCE\n";
  else
    cout<<"NOCHANCE\n";

  // debug(n);
  return ;
}

int main() {
  ios_base::sync_with_stdio(false);
  cin.tie(NULL);
  int t = 1;
  cin>>t;
  while(t--){
    solve();
  }
  return 0;
}