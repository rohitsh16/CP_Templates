```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 1000005;

string s; int n;

struct node{
  int open=0,
  close=0,
  done=0;
  void merge(node &a, node &b){
    int store = min(a.open, b.close);
    done += a.done + b.done + 2*store;
    open = a.open + b.open-store;
    close = a.close + b.close-store;
  }
};

vector<node> seg(1<<22);
node nullnode;

void build(int start, int end, int v=1){
  if(start==end){
    if(s[start]=='(')
      seg[v].open = 1;
    else
      seg[v].close = 1;
    return;
  }
  int mid = (start+end)>>1;
  build(start, mid, v*2);
  build(mid+1, end, v*2+1);
  seg[v].merge(seg[v*2], seg[v*2+1]);
}

node query(int l, int r, int v=1, int start=0, int end=n-1){
  if(r<start||l>end)
    return nullnode;
  if(l<=start&&r>=end)
    return seg[v];
  int mid = (start+end)>>1;
  node q1 = query(l, r, v*2, start, mid);
  node q2 = query(l, r, v*2+1, mid+1, end);
  node ans; ans.merge(q1, q2);
  return ans;
}

void solve(){
  cin>>s;
  int q; cin>>q;
  n = s.length();
  build(0, n-1);
  while(q--){
    int l,r; 
    cin>>l>>r;l--, r--;
    node res = query(l, r);
    cout<<res.done<<"\n";
  }
  return ;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t = 1;
  // cin>>t;
  while(t--){
    solve();
  }
  return 0;
}
