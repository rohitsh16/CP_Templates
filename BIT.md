```cpp
#include<bits/stdc++.h>
#include<ext/pb_ds/assoc_container.hpp>
#include<ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define pr pair<int,int>
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 1000005;

template <typename T>
using ordered_set = tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
#define ook order_of_key
#define fbo find_by_order
// ordered_set <int> s  ;
// auto x=s.find_by_order(k);
// s.order_of_key(val);

vector<int> bit(MAX_N,0);

void init(vector<int> a, int n){
  
  fill(all(bit),0);
  for(int i=1;i<=n;i++){
    int k=i;
    int x=bit[i-1];
    while(k<=n){
      bit[k]+=x;
      k+=(k&(-k));
    }
  }
  return ;
}

int query(int r, int n){
  int ans=0;
  int idx=r+1;
  while(idx>0){
    ans+=bit[idx];
    idx-=(idx&(-idx));
  }
  return ans;
}

void update(int idx, int val, int n){
  int pos=idx+1;
  while(pos<=n){
    bit[pos]+=val;
    pos+=(pos&(-pos));
  }
  return ;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
    int t=1;
    // cin>>t;
    while(t--){
      
    }
    return 0;
}
