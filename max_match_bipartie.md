```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(x) x.begin(),x.end()
#define mod 1000000007
int n, m, k;

struct Hopcroft_Karp{
  static const int inf = 1e9;
  int n;
  vector<int> matchL, matchR, dist;
  vector<vector<int> > g;
  Hopcroft_Karp(int n) :
    n(n), matchL(n+1), matchR(n+1), dist(n+1), g(n+1) {}

  void addEdge(int u, int v){
    g[u].push_back(v);
  }

  bool bfs(){
    queue<int> q;
    for(int u=1;u<=n;u++){
      if(!matchL[u]){
        dist[u]=0;
        q.push(u);
      }
      else
        dist[u]=inf;
    }
    dist[0]=inf;

    while(!q.empty()){
      int u=q.front();
      q.pop();
      for(auto v:g[u]){
        if(dist[matchR[v]] == inf){
          dist[matchR[v]] = dist[u] + 1;
          q.push(matchR[v]);
        }
      }
    }
    return (dist[0]!=inf);
  }

  bool dfs(int u){
    if(!u)
      return true;
    for(auto v:g[u]){
      if(dist[matchR[v]] == dist[u]+1 &&dfs(matchR[v])){
        matchL[u]=v;
        matchR[v]=u;
        return true;
      }
    }
    dist[u]=inf;
    return false;
  }

  int max_matching(){
    int matching=0;
    while(bfs()){
      for(int u=1;u<=n;u++){
        if(!matchL[u])
          if(dfs(u))
            matching++;
      }
    }
    return matching;
  }
};

void solve(){
    
  Hopcroft_Karp flow(5001);

  // add edges

  cout<<flow.max_matching()<<"\n";
  return ;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t = 1; 
  cin>>t;
  while(t--){
    solve();
  }
  return 0;
}