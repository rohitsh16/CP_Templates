```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 1000005;

const int dx[8]={1, 0, -1, 0, 1, 1, -1, -1}, dy[8]={0, 1, 0, -1, -1, 1, -1, 1};

int n,m;

bool check(int x, int y){
  return (x>=0&&x<n&&y>=0&&y<m);
}
vector<vector<bool> > vis(1001, vector<bool> (1001,0));

void dfs(vector<string> s, int x, int y){
  if(vis[x][y]||(!check(x,y))||s[x][y]=='#')
    return ;
  vis[x][y]=1;
  for(int i=0;i<4;i++){
    int xc=x+dx[i], yc=y+dy[i];
    if(check(xc, yc)&&!vis[xc][yc])
      dfs(s, xc, yc);
  }
  return;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t=1;
  // cin>>t;
  while(t--){
    cin>>n>>m;
    vector<string> s(n);
    for(int i=0;i<n;i++)
      cin>>s[i];

    int ans=0;
    for(int i=0;i<n;i++){
      for(int j=0;j<m;j++)
        if(s[i][j]=='.'&&!vis[i][j])
          dfs(s, i, j), 
          ans++;
    }
    cout<<ans<<"\n";
  }
  return 0;
}
