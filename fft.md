```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
#define PI 3.14
const ll INF = 1LL << 60;
const int MAX_N = 1000005;

typedef complex<long double> base;

void fft(vector<base> &a, bool invert){
  int n = (int)a.size();
  if(n==1)
    return ;
  vector<base> y0(n/2), y1(n/2);
  for(int i = 0, j = 0; i < n; i+=2, j++){
    y0[j] = a[i];
    y1[j] = a[i+1];
  }
  fft(y0, invert);
  fft(y1, invert);
  double ang = 2*PI/n*(invert?-1:1);
  base w(1), wn(cos(ang), sin(ang));
  for(int k = 0; k < n/2; k++){
    a[k] = y0[k] + w*y1[k];
    a[k+n/2] = y0[k] - w*y1[k];
    if(invert)
      a[k] /= 2, a[k+n/2] /= 2;
    w *= wn;
  }
}

void multiply(const vector<int> &a, const vector<int> &b, vector<int> &res){
  vector<base> fx (all(a)), fy(all(b)), hx;
  size_t n=1;
  int a_size = a.size(), b_size = b.size();
  int mx = a_size + b_size - 1;
  while(n < mx){
    n <<= 1;
  }
  fx.resize(n), fy.resize(n), hx.resize(n);

  fft(fx, 0), fft(fy, 0);

  for(size_t i = 0; i < n; i++){
    hx[i] = fx[i]*fy[i];
  }

  fft(hx, 1);
  res.resize(n);

  for(size_t i = 0; i < n; i++){
    res[i] = int(hx[i].real() + 0.5);
  }
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  vector<int> fx = {1, 3, 1};
  vector<int> gx = {1, 3};
  vector<int> res;
  multiply(fx, gx, res);
  int n = res.size();
  for(int i = 0; i < n; i++){
    cout<<res[i]<<" ";
  }
  cout<<"\n";
  return 0;
}
