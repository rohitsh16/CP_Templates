```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 100005;

vector<vector< pair<int, int> > > adj(MAX_N);
vector<bool> vis(MAX_N,0);
vector<ll> dis(MAX_N, INF);

void dijkstra(int s){
	dis[s] = 0;
	priority_queue<pair<int,int>, vector<pair<int,int> >, less<pair<int,int>> > pq;
	pq.push(mk(0,s));
	while(!pq.empty()){
		int u = pq.top().second;
		pq.pop();
		if(vis[u])
			continue;
		vis[u] = 1;
		for(auto i:adj[u]){
			if(dis[i.first]>dis[u]+i.second)
				dis[i.first] = dis[u]+i.second;
				pq.push(mk(dis[i.first],i.first));
			}
	}
	return ;
}

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t = 1;
  // cin>>t;
  while(t--){
    int n, m; cin>>n>>m;
    for(int i=0; i<n; i++){
    	int a, b, c; cin>>a>>b>>c;
    	a--, b--;
    	adj[a].eb(mk(b, c));
    	adj[b].eb(mk(a, c));
    }

    dijkstra(0);

    for(int i=0; i<n; i++)
    	cout<<dis[i]<<" ";
    
    cout<<"\n";
  }	
  return 0;
}
