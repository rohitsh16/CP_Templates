```cpp
#include<bits/stdc++.h>
using namespace std;
#define ll long long int
#define eb emplace_back
#define mk make_pair
#define all(arr) arr.begin(),arr.end()
#define mod 1000000007
const ll INF = 1LL << 60;
const int MAX_N = 1000005;

int main(){
  ios_base::sync_with_stdio(false);
  cin.tie(0);
  int t=1;
  // cin>>t;
  while(t--){
    int n; cin>>n;
    set<pair<int, int> > s;
    for(int i=0; i<n; i++){
    	int x, y; cin>>x>>y;
    	s.insert({x, 1}); // {2*x, 1} if times aren't distinct
    	s.insert({y+1, -1}); 
    }
    int ans = 0, c = 0;
    for(auto it:s){
    	c += it.second;
    	ans = max(c, ans);
    }
    cout<<ans<<"\n";
  }
  return 0;
}
